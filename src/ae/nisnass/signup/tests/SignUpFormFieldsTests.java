package ae.nisnass.signup.tests;

import static utils.Constants.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ae.nisnass.pages.HomePage;
import ae.nisnass.pages.SignupPage;
import utils.ScreenShot;
import utils.WebDriverFactory;

public class SignUpFormFieldsTests {
	public RemoteWebDriver driver;
	public SignupPage signupPage;
	public HomePage homePage;

	@Parameters({"browserName","hubURL","environment"})
	@BeforeClass
	public void setup(String browserName, String hubURL, String environment) throws IOException {

		WebDriverFactory webFactory = new WebDriverFactory();
		driver = webFactory.getWebDriver(browserName, hubURL, environment);
		driver.get(WEBSITE);
		WebDriverWait check = new WebDriverWait(driver, 100, 1000);
		check.until(ExpectedConditions.presenceOfElementLocated(By.className(NISNASS_LOGO_CLASSNAME_LOCATOR)));
		homePage = PageFactory.initElements(driver, HomePage.class);
		signupPage = PageFactory.initElements(driver, SignupPage.class);
		driver.manage().window().maximize();
		signUpByClick();

	}

	public void signUpByClick() throws IOException {
		homePage.ClickAccount();
		homePage.ClickSignUpButton();
	}

	@Test(description = "Validate all fields in SignUp page are enabled")
	public void ValiateIfFieldsAreEnabled() {

		Assert.assertTrue(signupPage.ValidateForm());
	}

	@Test(dataProvider = "names")
	public void ValidateNames(String firstName, String lastName) throws IOException {
		signupPage.EnterFirstName(firstName);
		signupPage.EnterLastName(lastName);
		Assert.assertEquals(signupPage.GetFirstName(), firstName);
		Assert.assertEquals(signupPage.GetLastName(), lastName);
	}

	@Test(description = "Validate data entered stays as it is.", dataProvider = "loginData")
	public void ValidateEmailAndPassword(String email, String password) throws IOException {

		signupPage = PageFactory.initElements(driver, SignupPage.class);
		signupPage.EnterEmail(email);
		signupPage.EnterPassowrd(password);
		Assert.assertEquals(signupPage.GetEmail(), email);
		Assert.assertEquals(signupPage.GetPassowrd(), password);
	}

	@Test(dataProvider = "negativeValuesName")
	public void ValidateNegativeCasesForNames(String className, ArrayList<String> values) throws IOException {


			if (className.contains("first")) {
				signupPage.EnterFirstName(values.get(0));
				Assert.assertEquals(signupPage.GetFirstNameValidation(), values.get(1), "Invalid Value in FirstName");
			} else {
				signupPage.EnterLastName(values.get(0));
				Assert.assertEquals(signupPage.GetLastNameValidation(), values.get(1), "Invalid Value in LastName");
			}
	

	}

	@Test(dataProvider = "negativePsw")
	public void ValidateNegativeCasesForEmailAndPassword(String className, ArrayList<String> values) throws IOException {
	
			if (className.contains("email")) {
				signupPage.EnterEmail(values.get(0));
				Assert.assertEquals(signupPage.GetEmailValidation(), values.get(1), "Invalid Value in email");
			} else {
				signupPage.EnterPassowrd(values.get(0));
				Assert.assertEquals(signupPage.GetPasswordValidation(), values.get(1), "Invalid Value in password");
			}
	

	}

	@Test(dataProvider = "updateNegativeNo")
	public void ValidateThatPhoneNumberAcceptsOnlyNumber(String number, String expected) throws IOException {
	
			signupPage.EnterPhone(number);
			Assert.assertEquals(signupPage.GetPhone(), expected, "Invalid phone number");
	
	}

	@Test(groups = { "signup" })
	public void validateAreaCode() {

		for (Map.Entry<String, String> entry : AREACODE.entrySet()) {
			signupPage.setAreaCode(entry.getValue());
			Assert.assertEquals(signupPage.GetAreaCode(), entry.getKey(), "Invalid area code");

		}
	}

	@Test(groups = { "signup" })
	public void validateCheckbox() throws IOException {
		Assert.assertFalse(signupPage.IsCheckboxEnabled());
		signupPage.ClickCheckbox();
		Assert.assertTrue(signupPage.IsCheckboxEnabled());
		signupPage.ClickCheckbox();
		Assert.assertFalse(signupPage.IsCheckboxEnabled());
	}

	@Test(groups = { "signup" })
	public void validateTermsCondition() throws IOException {

		Assert.assertEquals(signupPage.GetTermsText(), LINK_VISIBLETEXT);
		Assert.assertEquals(signupPage.GetTermsURL(), LINK_URL);
		signupPage.ClickTermsURL();
		Assert.assertEquals(driver.getCurrentUrl(), LINK_URL);
		WebDriverWait check = new WebDriverWait(driver, 100, 1000);
		check.until(ExpectedConditions.presenceOfElementLocated(By.className("WebStaticPage-staticTextBlock")));
		signUpByClick();

	}

	@Test(groups = { "signup" }) // (groups = { "signup" }, dependsOnMethods = { "validateAllElements" })
	public void validateEmptyValue() throws IOException {
		signupPage.EnterFirstName("");
		signupPage.EnterLastName("");
		signupPage.EnterEmail("");
		signupPage.EnterPassowrd("");
		signupPage.EnterPhone("");
		Assert.assertEquals(signupPage.GetFirstNameValidation(), FIRSTNAME_INVALID_ERROR);
		Assert.assertEquals(signupPage.GetLastNameValidation(), LASTNAME_INVALID_ERROR);
		Assert.assertEquals(signupPage.GetEmailValidation(), EMAIL_REQUIRED_ERROR);
		Assert.assertEquals(signupPage.GetPasswordValidation(), PSW_ERROR_MSG);
		Assert.assertEquals(signupPage.GetPhoneValidation(), PHONENO_ERROR_MSG);
	}

	@Test(dataProvider = "invalidNo")
	public void ValidateEmptyPhoneNumber(String number) throws IOException {
		signupPage.EnterPhone(number);
		Assert.assertEquals(signupPage.GetPhoneValidation(), PHONENO_ERROR_MSG);
	}

	@Test(alwaysRun = true) // dependsOnGroups= { "signup.*" },
	public void validateExisitingAccount() throws InterruptedException, IOException {
		signupPage.SignUp("John", "Smith", "vani1@gmail.com", "11234567897", "567890123");
		Thread.sleep(10000);
		Assert.assertEquals(signupPage.GetEmailReuseErrorDivValidation(), EMAIL_ERROR_MSG);

	}

	@DataProvider(name = "names")
	public Object[][] provideData() {

		return new Object[][] { { "John", "John" }, { "JOHN", "JOHN" }, { "john", "john" }, { "J", "john" },
				{ "JohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohn",
						"JohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohn" },
				{ "فانيسا", "فانيسا" }, { "ÉÖÜß", "ÉÖÜß" }

		};
	}

	@DataProvider(name = "loginData")
	public Object[][] provideEmailData() {
		return new Object[][] {

				{ "abc@gmail.com", "JohnSmith@89" }, { "abc@ymail.com", "12345678" },
				{ "abc@hotmmail.com", "JOHNSMITH" }, { "abc@rediffmail.com", "johnsmith" },

		};
	}

	@DataProvider(name = "negativeValuesName")
	public Object[][] provideNegativeData() {

		return new Object[][] { { "first name", new ArrayList<String>() {
			{
				add(" ");

				add(FIRSTNAME_INVALID_ERROR);

			}
		} }, { "first name", new ArrayList<String>() {
			{
				add("");

				add(FIRSTNAME_INVALID_ERROR);

			}
		} },
//			{ FIRST_NAME_CLASSNAME_LOCATOR, new ArrayList<String>() {{
//				add("123");
//			 
//			    add(FIRSTNAME_INVALID_ERROR);
//			 
//			}}
// },

				{ "last name", new ArrayList<String>() {
					{
						add(" ");

						add(LASTNAME_INVALID_ERROR);

					}
				} }, { "last name", new ArrayList<String>() {
					{
						add("");

						add(LASTNAME_INVALID_ERROR);

					}
				} },
//			{ LAST_NAME_CLASSNAME_LOCATOR, new ArrayList<String>() {{
//				add("123");
//		
//			    add(LASTNAME_INVALID_ERROR);
//			 
//			}}
// }

		};

	}

	@DataProvider(name = "negativePsw")
	public Object[][] provideNegativePsw() {

		return new Object[][] { { "password", new ArrayList<String>() {
			{
				add("123");
				add(PSW_INVALID_ERROR);
			}
		} }, { "password", new ArrayList<String>() {
			{
				add(" ");
				add(PSW_INVALID_ERROR);
			}
		} }, { "email", new ArrayList<String>() {
			{
				add("abc@gmail.c ");
				add(EMAIL_INVALID_ERROR);
			}
		} }, { "email", new ArrayList<String>() {
			{
				add("abc ");
				add(EMAIL_INVALID_ERROR);
			}
		} }, };
	}

	@DataProvider(name = "updateNegativeNo")
	public Object[][] negativeNo() {

		return new Object[][] { { "abc", "" },

				{ "675$$", "675" }, { "675$$897", "675897" }, { "675abc", "675" }, { "675abc345a7861", "6753457861" },
				{ "+675abc345a7861", "6753457861" } };
	}

	@DataProvider(name = "invalidNo")
	public Object[] invalidNo() {
		return new Object[] { " ", "", "" };

	}
	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			try {
				ScreenShot.takeSnapShot(driver,result.getName());
				System.out.println("Screenshot taken");
			} catch (Exception e) {
				System.out.println("Exception while taking screenshot " + e.getMessage());
			}
		}

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
