package ae.nisnass.signup.tests;

import static utils.Constants.*;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ae.nisnass.pages.HomePage;
import ae.nisnass.pages.MyAccountPage;
import ae.nisnass.pages.SignupPage;
import utils.ScreenShot;
import utils.WebDriverFactory;

public class SignUpTests {
	public RemoteWebDriver driver;
	public ae.nisnass.pages.SignupPage signupPage;
	public ae.nisnass.pages.MyAccountPage myAccountPage;

	@Parameters({ "browserName", "hubURL", "environment" })
	@BeforeClass
	public void setup(String browserName, String hubURL, String environment) throws IOException {
		WebDriverFactory webFactory = new WebDriverFactory();
		driver = webFactory.getWebDriver(browserName, hubURL, environment);
		driver.get(WEBSITE);
		WebDriverWait check = new WebDriverWait(driver, 100, 1000);
		check.until(ExpectedConditions.presenceOfElementLocated(By.className(NISNASS_LOGO_CLASSNAME_LOCATOR)));
		driver.manage().window().maximize();

		// When signing up a new account - Newly added test if it works
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.ClickAccount();
		homePage.ClickSignUpButton();
	}

	@Test(description = "validate if account creation works")
	public void validateCreateAccount() throws InterruptedException, IOException {

		signupPage = PageFactory.initElements(driver, SignupPage.class);
		Thread.sleep(5000);
		String email ="kantham"+(int)(Math.random()*9000)+1000+"@gmail.com";
		signupPage.SignUp("John", "Smith",email , "180874562923", "567890123");
		WebDriverWait check = new WebDriverWait(driver, 100, 1000);
		Thread.sleep(5000);
		signupPage.ClickAccountIcon();
		check.until(ExpectedConditions.presenceOfElementLocated(By.className("MyAccountPage-title")));
		myAccountPage = PageFactory.initElements(driver, MyAccountPage.class);
		Thread.sleep(5000);
		Assert.assertEquals(myAccountPage.getAccountFirstName(),"John");
		Assert.assertEquals(myAccountPage.getAccountLastName(),"Smith");
		Assert.assertEquals(myAccountPage.getAccountPhoneNo(),"+971 567890123");
		Assert.assertEquals(myAccountPage.getAccountEmail(),email);
	}

	@Test(description = "Validate if fields are enabled or not in update profile page")
	public void validateProfileUpdate() throws IOException {
		myAccountPage.ClickEditProfile();
		myAccountPage = PageFactory.initElements(driver, MyAccountPage.class);
		Assert.assertTrue(myAccountPage.IsFirstNameEnabled());
		Assert.assertTrue(myAccountPage.IsLastNameEnabled());
		Assert.assertTrue(myAccountPage.IsPhoneEnabled());
		Assert.assertFalse(myAccountPage.IsEmailEnabled());
		Assert.assertTrue(myAccountPage.IsUpdateButtonEnabled());

	}

	@Test(description = "Validate that the values in profile mathes the given values", dependsOnMethods = {
			"validateProfileUpdate" })
	public void UpdateProfile() throws InterruptedException, IOException {
		myAccountPage.UpdateProfile("UpdateFNAME", "UpdateFNAME", "", "67324238");
		Thread.sleep(1000);
		Assert.assertEquals(myAccountPage.GetFirstName(), "UpdateFNAME");
		Assert.assertEquals(myAccountPage.GetLastName(), "UpdateFNAME");
		Assert.assertEquals(myAccountPage.GetPhone(), "67324238");

	}

	@Test(dataProvider = "updateNegativeNo", dependsOnMethods = { "UpdateProfile" })
	public void ValidateThatPhoneNumberAcceptsOnlyNumber(String number, String expected) throws IOException {

		myAccountPage.EnterPhone(number);
		Assert.assertEquals(myAccountPage.GetPhone(), expected);
	}

	@Test(description = "Tries to update the profile with a empty number", dependsOnMethods = { "UpdateProfile" })
	public void updateNullNo() throws IOException {
		myAccountPage.PhoneFiledACtions(Keys.CONTROL + "a");
		myAccountPage.PhoneFiledACtions(Keys.DELETE);
		myAccountPage.PhoneFiledACtions(Keys.TAB);
		Assert.assertEquals(myAccountPage.GetPhoneValidation(), PHONENO_ERROR_MSG);
		Assert.assertFalse(myAccountPage.IsUpdateButtonEnabled());
	}

	@DataProvider(name = "updateNegativeNo")
	public Object[][] negativeNo() {
		return new Object[][] { { "675$$", "675" }, { "675$$897", "675897" }, { "675abc", "675" },
				{ "675abc345a7861", "6753457861" }, { "+675abc345a7861", "6753457861" } };
	}

	@AfterClass
	public void teardown() {
		driver.quit();
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			try {
				ScreenShot.takeSnapShot(driver,result.getName());
				System.out.println("Screenshot taken");
			} catch (Exception e) {
				System.out.println("Exception while taking screenshot " + e.getMessage());
			}
		}

	}
}
