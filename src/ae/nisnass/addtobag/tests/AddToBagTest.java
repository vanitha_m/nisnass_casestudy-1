package ae.nisnass.addtobag.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ae.nisnass.pages.BagItems;
import ae.nisnass.pages.HomePage;
import ae.nisnass.pages.ProductPage;
import utils.ScreenShot;
import utils.WebDriverFactory;
import static utils.Constants.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AddToBagTest {

	private RemoteWebDriver driver;
	public HomePage homePage;
	public ProductPage productPage;
	public BagItems bagItems;
	private String userName="vani1@gmail.com",
			pwd="12345678";

	@Parameters({"browserName","hubURL","environment"})
	@BeforeClass
	public void setup(String browserName,String hubURL,String environment) throws IOException {
		WebDriverFactory webFactory = new WebDriverFactory();
		//Using a differnet user name for chrome
		if(browserName.equals("chrome"))
		{
			userName = "vani2@gmail.com";
		}
		driver = webFactory.getWebDriver(browserName,hubURL,environment);
	}

	@DataProvider(name = "BagItems")
	public Object[][] provideEmptyValue() {

		return new Object[][] {
				{ "https://www.nisnass.ae/shop-quench-intense-hydration-mask-1-sheet-for-womens-212054000.html",
						new ArrayList<String>() {
							{
								add(null);
								add(null);
							}
						} },
				{ "https://www.nisnass.ae/shop-zitah-classic-pumps-for-womens-212061761.html", new ArrayList<String>() {
					{
						add("color_Pink");
						add("6.5");
					}
				} },
				{ "https://www.nisnass.ae/shop-metallic-sandals-for-womens-212224468.html", new ArrayList<String>() {
					{
						add(null);
						add("40");
					}
				} }, };
	}

	@Test(description = "Login to website")
	public void loginToWebsite() throws IOException, InterruptedException {
		try {
			driver.get(WEBSITE);
			WebDriverWait check = new WebDriverWait(driver, 100, 1000);
			check.until(ExpectedConditions.presenceOfElementLocated(By.className(NISNASS_LOGO_CLASSNAME_LOCATOR)));
			driver.manage().window().maximize();
			homePage = PageFactory.initElements(driver, HomePage.class);
			productPage = PageFactory.initElements(driver, ProductPage.class);
			bagItems = PageFactory.initElements(driver, BagItems.class);
			homePage.ClickAccount();
			homePage.setEmailID(userName);
			homePage.setEmailPsw(pwd);
			homePage.signInBtn();
			bagItems.removeItems("https://www.nisnass.ae/cart");
			//Assert.assertEquals(true, "Login To Website Success");
		} catch (Exception e) {
			// TODO Auto-generated catch block
		   // Assert.assertEquals(false, "Login To Website Failed");
			e.printStackTrace();
		}
	}

	@Test(description = "Add item to bag", dataProvider = "BagItems", dependsOnMethods = { "loginToWebsite" })
	public void addToBag(String url, List<String> products) throws InterruptedException, IOException {
		try {
			driver.get(url);
			Thread.sleep(10000);
			if (products.get(0) != null) {
				productPage.selectColor(products.get(0));
				Thread.sleep(1000);
			}
			if (products.get(1) != null) {
				productPage.setSize(products.get(1));
				Thread.sleep(1000);
			}
			productPage.clickAddToBag();
			//Assert.assertEquals(true, "Successfully added product in URL" + url);
		} catch (Exception e) {
			Assert.assertEquals(false, "Failed to added product in URL" + url);
			e.printStackTrace();
		}
	}

	@DataProvider(name = "ValidateBagItems")
	public Object[][] validateBagItems() throws IOException, InterruptedException {

		productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickBagIcon();

		return new Object[][] { { "Quench Intense Hydration Mask, 1 sheet", new ArrayList<String>() {
			{
				add("Neutral");
				add("ONE SIZE");
				add("1");
			}
		} }, { "Zitah Classic Pumps", new ArrayList<String>() {
			{
				add("Pink");
				add("6.5");
				add("1");
			}
		} }, { "Metallic Sandals", new ArrayList<String>() {
			{
				add("Silver");
				add("40");
				add("1");
			}
		} }, };
	}

	@Test(description = "Validate if the bag has items that was selected", dataProvider = "ValidateBagItems", dependsOnMethods = {
			"addToBag" })
	public void validateBag(String productName, List<String> itemDetails) throws InterruptedException {
		Thread.sleep(2000);
		Assert.assertEquals(bagItems.getTotalUniqueItems(), 3);
		List<String> bagItemPname = bagItems.getProductName();
		Assert.assertTrue(bagItemPname.contains(productName));
		Assert.assertEquals(bagItems.getTotalUniqueItems(), 3);
		Assert.assertEquals(bagItems.getColor(productName), itemDetails.get(0));
		Assert.assertEquals(bagItems.getSize(productName), itemDetails.get(1));
		Assert.assertEquals(bagItems.getCounter(productName), itemDetails.get(2));
	}

	@Test(description="Validate items out of stock", dependsOnMethods = { "validateBag" })
	public void validateOutOfStockItems() throws IOException, InterruptedException {
		driver.get("https://www.nisnass.ae/shop-metallic-sandals-for-womens-212224468.html");
		productPage.setSize("40");
		productPage.clickAddToBag();
		productPage.clickBagIcon();
		Assert.assertEquals(bagItems.getCounter("Metallic Sandals"), "1");
	}

	@Test(dependsOnMethods = { "validateOutOfStockItems" })
	public void validateMoreThanSixItemsAddition() throws IOException, InterruptedException {
		driver.get("https://www.nisnass.ae/shop-quench-intense-hydration-mask-1-sheet-for-womens-212054000.html");
		for (int i = 0; i < 6; i++) {
			productPage.clickAddToBag();
		}
		productPage.clickBagIcon();
		Assert.assertEquals(bagItems.getCounter("Quench Intense Hydration Mask, 1 sheet"), "6");
	}

	@Test(dependsOnMethods = { "validateMoreThanSixItemsAddition" })
	public void validateAddBagFailure() throws IOException, InterruptedException {
		driver.get("https://www.nisnass.ae/shop-zitah-classic-pumps-for-womens-212061761.html");
		productPage.clickAddToBag();
		productPage.clickBagIcon();
		Assert.assertEquals(bagItems.getCounter("Zitah Classic Pumps"), "1");
	}

	@AfterClass
	public void tearDown() throws IOException {
		if (driver != null) {
			driver.quit();
		}
	}
	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			try {
				ScreenShot.takeSnapShot(driver,result.getName());
				System.out.println("Screenshot taken");
			} catch (Exception e) {
				System.out.println("Exception while taking screenshot " + e.getMessage());
			}
		}

	}

}
