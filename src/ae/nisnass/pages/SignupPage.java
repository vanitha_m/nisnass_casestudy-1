package ae.nisnass.pages;


import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignupPage {
	WebDriver driver;

	public SignupPage(WebDriver driver) {
		this.driver = driver;

	}

	@FindBy(className = "Profile-firstName")
	WebElement FirstName;
	@FindBy(className = "Profile-lastName")
	WebElement LastName;
	@FindBy(className = "Profile-email")
	WebElement Email;
	@FindBy(name = "password")
	WebElement Password;
	@FindBy(className = "Profile-phoneNumber")
	WebElement Phone;
	@FindBy(className = "Select-value")
	WebElement AreaCode;
	@FindBy(xpath = ".//*[@id='root']/div/div[1]/div[3]/div/div[2]/div[1]/form/div[2]/div[1]")
	WebElement FirstNameValidationDiv;
	@FindBy(xpath = ".//*[@id='root']/div/div[1]/div[3]/div/div[2]/div[1]/form/div[2]/div[2]")
	WebElement LastNameValidationDiv;
	@FindBy(className = "Register-termsConditionsLink")
	WebElement TermsAndConditionsLink;
	@FindBy(xpath=".//*[@id='root']/div/div[1]/div[3]/div/div[2]/div[1]/form/div[4]")
	WebElement PasswordErrorDiv;
	@FindBy(xpath=".//*[@id='root']/div/div[1]/div[3]/div/div[2]/div[1]/form/div[3]")
	WebElement EmailErrorDiv;
	@FindBy(xpath=".//*[@id='root']/div/div[1]/div[3]/div/div[2]/div[1]/form/div[6]")
	WebElement PhoneNumberErrorDiv;
	@FindBy(className="Select-value-label")
	WebElement AreaCodeValue;
	@FindBy(id="Profile-subscribe")
	WebElement PromotionalCheckBox;
    @FindBy(className="Register-termsConditionsLink")
	WebElement TermsAndConditionLink;
	@FindBy(className="Profile-signUpButton")
	WebElement SignUpButton;
	
	@FindBy(className="Popup-iconText")
	WebElement AccountIcon;
	@FindBy(xpath=".//*[@id='root']/div/div[1]/div[3]/div/div[2]/div[1]/form/div[7]")
	WebElement EmailReuseErrorDiv;
	
	public void EnterFirstName(String firstName) throws IOException {
		FirstName.sendKeys(Keys.CONTROL + "a");
		FirstName.sendKeys(Keys.DELETE);
		FirstName.sendKeys(firstName);
		FirstName.sendKeys(Keys.TAB);
	}

	public String GetFirstName() {
		return FirstName.getAttribute("value");
	}

	public String GetFirstNameValidation() {
		return FirstNameValidationDiv.getText();
	}

	
	public void EnterLastName(String lastName) throws IOException {
		LastName.sendKeys(Keys.CONTROL + "a");
		LastName.sendKeys(Keys.DELETE);
		LastName.sendKeys(lastName);
		LastName.sendKeys(Keys.TAB);
	}

	public String GetLastName() {
		return LastName.getAttribute("value");
	}

	public String GetLastNameValidation() {
		return LastNameValidationDiv.getText();
	}

	public void EnterEmail(String email) throws IOException {
		Email.sendKeys(Keys.CONTROL + "a");
		Email.sendKeys(Keys.DELETE);
		Email.sendKeys(email);
		Email.sendKeys(Keys.TAB);
	}

	public String GetEmail() {
		return Email.getAttribute("value");
	}

	public String GetEmailValidation() {
		return EmailErrorDiv.getText();
	}
	public void EnterPassowrd(String password) throws IOException {
		Password.sendKeys(Keys.CONTROL + "a");
		Password.sendKeys(Keys.DELETE);
		Password.sendKeys(password);
		Password.sendKeys(Keys.TAB);
		
	}

	public String GetPassowrd() {
		return Password.getAttribute("value");
	}
	public String GetPasswordValidation() {
		return PasswordErrorDiv.getText();
	}
	public void EnterPhone(String phone) throws IOException {
		Phone.sendKeys(Keys.CONTROL + "a");
		Phone.sendKeys(Keys.DELETE);
		Phone.sendKeys(phone);
		Phone.sendKeys(Keys.TAB);
	}

	public String GetPhone() {
		return Phone.getAttribute("value");
	}
	public String GetPhoneValidation() {
		return PhoneNumberErrorDiv.getText();
	}

	public void setAreaCode(String areaCode) {
		 AreaCode.click();
		 String areaCodeValue = "div[aria-label='"+ areaCode+ "']";
         driver.findElement(By.cssSelector(areaCodeValue)).click();
	}

	public String GetAreaCode() {
		return AreaCodeValue.getText();
	}
	

	public Boolean ValidateForm() {
		return FirstName.isEnabled() && LastName.isEnabled() 
				&& Email.isEnabled() && Password.isEnabled()
				&& Phone.isEnabled() && AreaCode.isEnabled()
				&& TermsAndConditionsLink.isEnabled();
				
	}
	public void ClickCheckbox() throws IOException
	{
		PromotionalCheckBox.click();
	}
	
	public Boolean IsCheckboxEnabled()
	{
		return PromotionalCheckBox.isSelected();
	}

	public String GetTermsText()
	{
		return TermsAndConditionLink.getText();
	}
	public String GetTermsURL()
	{
		return TermsAndConditionLink.getAttribute("href");
	}
	public void ClickTermsURL() throws IOException
	{
		TermsAndConditionLink.click();
	}
	
	public void SignUp(String firstName,String lastName, String email,String password,String phoneNumber) throws IOException
	{
		this.EnterFirstName(firstName);
		this.EnterLastName(lastName);
		this.EnterEmail(email);
		this.EnterPassowrd(password);
		this.EnterPhone(phoneNumber);
		SignUpButton.click();
	}
	
	public void ClickAccountIcon() throws IOException
	{
		AccountIcon.click();
	}
	
	public String GetEmailReuseErrorDivValidation() {
		return EmailReuseErrorDiv.getText();
	}
	
}
