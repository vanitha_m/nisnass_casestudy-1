package ae.nisnass.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BagItems {
    
    WebDriver driver;
    
    public BagItems(WebDriver driver)
    {
        
        this.driver= driver;
    }
    
    @FindBy(className = "CartItem-content")
    List<WebElement> cartItems;
    @FindBy(className = "CartItem-info")
    List<WebElement> cartItemsInfo;
    @FindBy(className = "CartItem-name")
    List<WebElement> cartItemsName;
    @FindBy(className = "CartItem-action")
    List<WebElement> removeItems;
    @FindBy(className = "ConfirmationModal-yesButton")
    WebElement okButton;
    String counter = ".//div[@class=\"Counter\"]/span";
    String cartItemXPath = ".//div[@class=\"CartItem-name\"]";
    String colorItemXPath = ".//div[@class=\"CartItem-attribute\"]/span[@class=\"CartItem-attributeName\" and contains(text(), \"Color\")]/..//span[@class=\"CartItem-attributeValue\"]";
    String sizeItemXpath = ".//div[@class=\"CartItem-attribute\"]/span[@class=\"CartItem-attributeName\" and contains(text(), \"Size\")]/..//span[@class=\"CartItem-attributeValue\"]";
    public int getTotalUniqueItems()
    {
        return cartItems.size();
    }
    
    public String getColor(String productName)
    {
    	return getItem(productName, colorItemXPath);
    }
    
    public String getSize(String productName)
    {
        return getItem(productName, sizeItemXpath);
    }
    
    public String getCounter(String productName)
    {
    	return getItem(productName, counter);
    }
    
    public List<String> getProductName()
    {
        List<String> productName = new ArrayList<String>();
        for (WebElement element : cartItemsName)
        {
            productName.add(element.getText());
        }
        return productName;
    }
    
    public void removeItems(String url) throws IOException, InterruptedException
    {
    	driver.get(url);
    	Thread.sleep(5000);
    	for(WebElement element : removeItems)
    	{
    		element.click();
    		Thread.sleep(2000);
    		okButton.click();
    		Thread.sleep(5000);
    	}
    }
    private String getItem(String productName, String itemType)
    {
    	for (WebElement element : cartItems)
        {
            if (element.findElement(By.xpath(cartItemXPath)).getText().contains(productName))
            {
            	return element.findElement(By.xpath(itemType)).getText();               
            }
        }
        return null;
    }
}