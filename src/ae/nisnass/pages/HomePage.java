package ae.nisnass.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage {

	WebDriver driver;
	
	public HomePage(WebDriver driver)
	{
		
		this.driver= driver;
	}
		

	@FindBy(className = "Popup-button")
	WebElement SignUpButton;
	@FindBy(className = "SignInForm-email")
	WebElement emailID;
	@FindBy(className = "SignInForm-password")
	WebElement emailPsw;
	@FindBy(className = "SignInForm-signInButton")
	WebElement signInBtn;
	@FindBy(className = "SignInForm-signUpButton")
	WebElement PopUpSignUpButton;
	
	public void setEmailID(String emailID) throws IOException
	{
	   this.emailID.clear();
	   this.emailID.sendKeys(emailID);
	}
	
	public void setEmailPsw(String emailPsw) throws IOException
	{
	   this.emailPsw.clear();
	   this.emailPsw.sendKeys(emailPsw);
	}
	
	public void signInBtn() throws IOException, InterruptedException
	{
	   this.signInBtn.click();
	   Thread.sleep(10000);
	}
	
	public void ClickAccount() throws IOException {
		SignUpButton.click();


	}
	
	public void ClickSignUpButton() throws IOException {
		PopUpSignUpButton.click();

	}
	
	
}
