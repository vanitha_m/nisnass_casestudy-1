package ae.nisnass.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyAccountPage {
	WebDriver driver;

	public MyAccountPage(WebDriver driver) {

		this.driver = driver;
	}

	@FindBy(name = "firstName")
	WebElement FirstName;
	@FindBy(name = "lastName")
	WebElement LastName;

	@FindBy(xpath = ".//*[@id='root']/div/div[1]/div[3]/div/div[2]/div[1]/form/div[2]/div[1]")
	WebElement FirstNameValidationDiv;
	@FindBy(xpath = ".//*[@id='root']/div/div[1]/div[3]/div/div[2]/div[1]/form/div[2]/div[2]")
	WebElement LastNameValidationDiv;
	@FindBy(className = "Profile-email")
	WebElement Email;
	@FindBy(className = "Profile-phoneNumber")
	WebElement Phone;
	@FindBy(className = "Profile-updateDetailsButton")
	WebElement Updatebutton;
	@FindBy(xpath = ".//*[@id=\"root\"]/div/div[1]/div[3]/div/div[2]/div/form[1]/div[5]")
	WebElement PhoneNumberErrorDiv;
	@FindBy(className = "Select-value-label")
	WebElement AreaCodeValue;
	@FindBy(className = "Select-value")
	WebElement AreaCode;
	@FindBy(xpath = ".//*[@id='root']/div/div[1]/div[3]/div/div[2]/div[3]/a")
	WebElement EditAccountButton;
	@FindBy(xpath =".//*[@id='root']/div/div[1]/div[3]/div/div[2]/div[4]/div[1]/div[1]/span[2]")
	WebElement accountFirstName;
	@FindBy(xpath =".//*[@id='root']/div/div[1]/div[3]/div/div[2]/div[4]/div[2]/div[1]/span[2]")
	WebElement accountLastName;
	@FindBy(xpath =".//*[@id='root']/div/div[1]/div[3]/div/div[2]/div[4]/div[1]/div[2]/span[2]")
	WebElement accountEmail;
	@FindBy(xpath =".//*[@id='root']/div/div[1]/div[3]/div/div[2]/div[4]/div[2]/div[2]/span[2]")
	WebElement accountPhoneNo;
	
	public String getAccountFirstName() {
		return accountFirstName.getText();
	}
	
	public String getAccountLastName() {
		return accountLastName.getText();
	}
	
	public String getAccountEmail() {
		return accountEmail.getText();
	}
	
	public String getAccountPhoneNo() {
		return accountPhoneNo.getText();
	}

	public void EnterFirstName(String firstName) throws IOException {
		FirstName.clear();
		FirstName.sendKeys(firstName);
		FirstName.sendKeys(Keys.TAB);
	}

	public String GetFirstName() {
		return FirstName.getAttribute("value");
	}

	public String GetFirstNameValidation() {
		return FirstNameValidationDiv.getText();
	}

	public Boolean IsFirstNameEnabled() {
		return FirstName.isEnabled();
	}

	public void EnterLastName(String lastName) throws IOException {
		LastName.clear();
		LastName.sendKeys(lastName);
		LastName.sendKeys(Keys.TAB);
	}

	public Boolean IsLastNameEnabled() {
		return LastName.isEnabled();
	}

	public String GetLastName() {
		return LastName.getAttribute("value");
	}

	public String GetLastNameValidation() {
		return LastNameValidationDiv.getText();
	}

	public String GetEmail() {
		return Email.getAttribute("value");
	}

	public Boolean IsEmailEnabled() {
		return Email.isEnabled();
	}

	public void EnterPhone(String phone) throws IOException {
		Phone.clear();
		Phone.sendKeys(phone);
		Phone.sendKeys(Keys.TAB);
	}

	public Boolean IsPhoneEnabled() {
		return Phone.isEnabled();
	}

	public String GetPhone() {
		return Phone.getAttribute("value");
	}

	public String GetPhoneValidation() {
		return PhoneNumberErrorDiv.getText();
	}

	public String GetAreaCode() {
		return AreaCodeValue.getText();
	}

	public void setAreaCode(String areaCode) throws IOException {
		AreaCode.click();
		String areaCodeValue = "div[aria-label='" + areaCode + "']";
		driver.findElement(By.cssSelector(areaCodeValue)).click();
	}

	public Boolean IsUpdateButtonEnabled() {
		return Updatebutton.isEnabled();
	}

	public void UpdateProfile(String firstName, String lastName, String areaCode, String phone) throws IOException {
		this.EnterFirstName(firstName);
		this.EnterLastName(lastName);
		this.EnterPhone(phone);
		Updatebutton.click();
	}

	public void PhoneFiledACtions(CharSequence c) throws IOException {
		Phone.sendKeys(c);
	}

	public void ClickEditProfile() throws IOException {
		EditAccountButton.click();
	}

}
