package ae.nisnass.pages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage {

	WebDriver driver;
	
	public ProductPage(WebDriver driver)
	{
		
		this.driver= driver;
	}
		
	@FindBy(className = "PDP-addToBag")
	WebElement addToBagButton;
	@FindBy(className = "icon-bag")
	WebElement clickBagIcon;
	@FindBy(xpath = "//div[@class=\"ColorSelection-options\"]/div/button/img")
	List<WebElement> colors;
	@FindBy(xpath = "//div[@class=\"SizeSelection-options\"]/button")
	List<WebElement> allSize;
	
	public void clickAddToBag() throws IOException, InterruptedException
	{
	   this.addToBagButton.click();
	   Thread.sleep(5000);
	}
	
	public void clickBagIcon() throws IOException, InterruptedException
	{
	   this.clickBagIcon.click();
	   Thread.sleep(5000);
	}
	
	public void selectColor(String color) throws IOException
	{
		for (WebElement element : colors)
		{
			if (element.getAttribute("alt").equals(color))
			{
				element.click();
				break;
			}
		}
	}
	
	public void setSize(String size) throws IOException
	{
		for (WebElement element : allSize)
		{
			if (element.getText().equals(size))
			{
				element.click();
				break;
			}
		}
	}
}
