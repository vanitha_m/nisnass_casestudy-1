package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ScreenShot {

	private static final String SCREENSHOT_FOLDER = "output/screenshots";


	public static void takeSnapShot(WebDriver webDriver, String methodName) throws IOException {
		try {
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
			File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
			try {
				String screenShotFolder = String.format("%s/%s", System.getProperty("user.dir"), SCREENSHOT_FOLDER);
				File destFile = new File((String) screenShotFolder + "/failure_screenshots/" + methodName + "_"
						+ formater.format(calendar.getTime()) + ".png");
				FileUtils.copyFile(scrFile, destFile);

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (WebDriverException e) {

			e.printStackTrace();
		}
	}
}