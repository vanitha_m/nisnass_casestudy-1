package utils;

import static utils.Constants.DRIVER_PATH;
import static utils.Constants.FIREFOX_DRIVER_PATH;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class WebDriverFactory {
	private RemoteWebDriver driver;
	public RemoteWebDriver getWebDriver(String browserName,String hubURL,String environment) throws MalformedURLException {
		DesiredCapabilities dr = null;
		Platform platfom = null;
		if (environment.equals("Windows"))
			platfom = org.openqa.selenium.Platform.WINDOWS;
		else
			platfom = org.openqa.selenium.Platform.LINUX;
		
		if (browserName.equals("chrome")) {
			System.setProperty("Webdriver.chrome.driver", DRIVER_PATH);
			dr = DesiredCapabilities.chrome();
			dr.setBrowserName("chrome");
			dr.setPlatform(platfom);
			
		} else {
			System.setProperty("Webdriver.firefox.marionette", "false");
			System.setProperty("Webdriver.firefox.driver", FIREFOX_DRIVER_PATH);
			dr = DesiredCapabilities.firefox();
			dr.setBrowserName("firefox");
			dr.setPlatform(platfom);
				}
		driver = new RemoteWebDriver(new URL(hubURL), dr);
		return driver;
	}
}
