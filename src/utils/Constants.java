package utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Constants {

	public static final String DRIVER_PATH = "src/libs/chromedriver.exe" ;
	public static final String FIREFOX_DRIVER_PATH = "src/libs/geckodriver.exe" ;
	public static final String WEBSITE = "https://www.nisnass.ae/";
	public static final String NISNASS_LOGO_CLASSNAME_LOCATOR = "image-nisnass-logo";
    public static final String SCREENSHOT_FOLDER = "output/screenshots";
	


    public static final String FIRSTNAME_INVALID_ERROR = "First Name is a required field";
    public static final String LASTNAME_INVALID_ERROR = "Last Name is a required field";
    public static final String EMAIL_INVALID_ERROR = "E-mail Address is invalid";
    public static final String EMAIL_REQUIRED_ERROR = "E-mail Address is a required field";
    public static final String PSW_INVALID_ERROR = "Minimum 6 characters";
    public static final String PSW_ERROR_MSG = "Password is a required field";
    public static final String PHONENO_ERROR_MSG = "Phone Number is a required field";
    
	public static final String PROMOTIONAL_CHECKBOX_ID_LOCATOR = "Profile-subscribe";
	public static final String AREACODE_VISIBLETEXT_CLASSNAME = "Select-value-label";
	public static final String LINK_VISIBLETEXT = "Terms & Conditions";
	public static final String LINK_URL ="https://www.nisnass.ae/content/termsandconditions";
	public static final String EMAIL_ERROR_MSG = "A customer with the same email already exists in an associated website.";

    public static final Map<String, String> AREACODE;
    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("+971", "United Arab Emirates (+971)");
        aMap.put("+91", "India (+91)");
        AREACODE = Collections.unmodifiableMap(aMap);
    }
    
    //Terms & Condition Page
	public static final String TERMS_CONDITION_PAGE_ELEMENT_CLASSNAME = "WebStaticPage-staticTextBlock";
    public static final String EMAIL_NON_EDITABLE=" (not editable)";
}